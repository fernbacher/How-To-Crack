# How to crack steam games AutoCracker method (ONLY THE ONES WITH STEAM DRM)

## First step
1. You download [SteamAutoCracker](https://github.com/BigBoiCJ/SteamAutoCracker)
2. You follow the instructions to set it up
3. Not necessary to set a folder in the first prompt, thats for dlc unlocking and more for games you already own
4. Set your folder with cracked games
5. Answer the prompts based on what you want
6. Crack the game you want providing its name / appid from [steamdb](https://steamdb.info/)
### Second step
1. Download games from [cs.rin.ru](https://cs.rin.ru/forum/viewforum.php?f=22)
2. Bypass gdrive limit by starring the game you want and starring something very small, and download them at the same time from the starred tab in gdrive, this way it has to zip the files so it bypasses the limit

# How to crack steam games with goldberg + steamless manually (ONLY THE ONES WITH STEAM DRM)
1. Download [GoldbergGUI](https://git.jeddunk.xyz/jeddunk/GoldbergGUI) / [GoldbergEMU (Manual)](https://mr_goldberg.gitlab.io/goldberg_emulator/)
2. Download [Steamless](https://github.com/atom0s/Steamless)
3. Open up Goldberg GUI, click select, select the steam_api / steam_api64 .dll from your games's folder, find id, get dlcs, set your name from the global settings tab or steamid, choose offline mode if you wish to, SAVE.
 - Normal Goldberg is about the same as GUI just that you do it manually, so if you like manual labour like some chinese factory worker, there is a readme file in the goldberg emulator's zip file
4. Open up Steamless, choose the .exe of your game, patch it, its all automated.
5. Run the game with the .exe created by steamless

``If you wish to crack from other platforms like socialclub, epic games, etc, you can go in an endless loop of info on`` [cs.rin.ru](https://cs.rin.ru/)